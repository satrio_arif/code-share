<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'editor', 'namespace' => 'Editor'], function()
{
	//Student
	Route::get('/', ['as' => 'editor.student.index', 'uses' => 'StudentController@index']);
	Route::post('/', ['as' => 'editor.student.store', 'uses' => 'StudentController@store']);
	Route::put('/{id}/update', ['as' => 'editor.student.update', 'uses' => 'StudentController@update']);
	Route::delete('/{id}/delete', ['as' => 'editor.student.delete', 'uses' => 'StudentController@delete']);
});