@extends('layouts.template')
@section('content')
<div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile" id="kt_page_portlet">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">
				Student List
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-actions">
					<a href="#add" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal">
						<i class="la la-plus"></i>
						Add New
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
			<thead>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($students as $key => $student)
					<tr>
						<td>{{$number++}}</td>
						{{-- <td>{{$student->name}}</td> --}}
						<td>
							<a href="" class="btn btn-link">{{$student->name}}</a>
						</td>
						<td>
							<button id="btnGroupVerticalDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								{{$student->name}}
							</button>
							<div class="dropdown-menu" aria-labelledby="btnGroupVerticalDrop1">
								<a class="dropdown-item" href="">Station</a>
								<a class="dropdown-item" href="">Analysis</a>
								<a class="dropdown-item" href="">Report & Map</a>
								<a class="dropdown-item" href="">Summary</a>
							</div>
						</td>
						<td>
							{!! Form::open(array('route' => ['editor.student.delete', $student->id], 'method' => 'delete'))!!}
							{{ csrf_field() }}
							<a href="#edit{{$student->id}}" class="btn btn-sm btn-primary btn-icon btn-icon-md" title="Edit" data-toggle="modal">
								<i class="la la-edit"></i>
							</a>

							<button type="submit" class="btn btn-sm btn-google btn-icon btn-icon-md" title="Delete"><i class="la la-trash-o"></i></a></button>
							{!! Form::close() !!}
						</td>
					</tr>
					
					{{-- BEGIN MODAL EDIT --}}
					<div class="modal fade" id="edit{{$student->id}}" tabindex="-1" role="basic" aria-hidden="true">
						<div class="modal-dialog">
							{!! Form::open(array('route' => ['editor.student.update', $student->id], 'method' => 'PUT'))!!}
							{{ csrf_field() }}
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Edit Student</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									</button>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label>Name</label>
										<input type="text" class="form-control" name="name" value="{{$student->name}}">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
					{{-- END MODAL EDIT --}}
				@endforeach
			</tbody>
		</table>

		<!--end: Datatable -->
	</div>

	{{-- BEGIN MODAL ADD --}}
	<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			{!! Form::open(array('route' => ['editor.student.store'], 'method' => 'POST'))!!}
			{{ csrf_field() }}
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add New Student</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input type="text" class="form-control" name="name">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" name="saveAdd" value="1">Save</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	{{-- END MODAL ADD --}}
</div>
@endsection