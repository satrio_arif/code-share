<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

	<!-- begin:: Aside -->
	<div class="kt-header__brand kt-grid__item  " id="kt_header_brand">
		<div class="kt-header__brand-logo">
			<a href="index.html">
				<img alt="Logo" src="{{asset('../assets/media/logos/logo-6.png')}}" />
			</a>
		</div>
	</div>
	<!-- end:: Aside -->

	<!-- begin:: Title -->
	<h3 class="kt-header__title kt-grid__item">
		SIDMS
	</h3>
	<!-- end:: Title -->

	<!-- begin: Header Menu -->
	<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
	<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
	{{-- @include('layouts.headerMenu') --}}
	</div>
	<!-- end: Header Menu -->

	<!-- begin:: Header Topbar -->
	@include('layouts.headerTopbar')
	<!-- end:: Header Topbar -->

</div>