<button class="kt-aside-close " id="kt_aside_close_btn">
	<i class="la la-close"></i>
</button>
<div class="kt-aside kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

	<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="kt-aside-menu kt-scroll ps" data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500" style="height: 683px; overflow: hidden;">
			<ul class="kt-menu__nav ">
				<li class="kt-menu__item " aria-haspopup="true" style="display: none;">
					<a href="javascript:;" class="kt-menu__link ">
						<i class="kt-menu__link-icon">ST</i>
						<span class="kt-menu__link-text">Steam Flow Rate</span>
					</a>
				</li>

				<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon">GL</i>
						<span class="kt-menu__link-text">Geology</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Surface</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="http://sidms.budimas.com/editor/glmapping" class="kt-menu__link">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Geological Mapping</span>
											</a>
										</li>
									</ul>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="http://sidms.budimas.com/editor/glhazard" class="kt-menu__link">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Geohazard</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
						</ul>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Sub Surface</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/well" class="kt-menu__link kt-menu__toggle">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Well Information</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/wellcasing" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Drilling Casing</span>
											</a>
										</li>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Drilling Data</span>
												<i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/welldirsurvey" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Directional Survey</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/wellparameter" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Drilling Parameter</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/wellevent" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Drilling Event</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/wellmudlog" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Mud Log</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Drilling Report</span>
												<i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/wellreportprognosis" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Prognosis</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/wellreportcompletion" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Completion</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/wellreportdaily" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Daily</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/wellreportwsgmorning" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">WSG</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Formation &amp; Evaluation</span>
												<i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/welldatacutting" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Data Cutting</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/welldatalogging" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Data Logging</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/welldatacoring" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Data Coring</span>
														</a>
													</li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Analysis Cutting</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu ">
															<span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscutting/1" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Lithology</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscutting/2" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Alteration</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscutting/3" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">XRD</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscutting/4" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Others</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="javascript:;" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">MEB</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Analysis Logging</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu ">
															<span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysislogging/1" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Lithology</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysislogging/2" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Petrophysics</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysislogging/3" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Fracture</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysislogging/4" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Others</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>													
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Analysis Coring</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu ">
															<span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscoring/1" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Lithology</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscoring/2" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Alteration</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscoring/3" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">XRD</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscoring/4" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Petrophysiscs</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscoring/5" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Fracture</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/wellanalysiscoring/6" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Others</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
														<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Final Interpretation</span>
															<i class="kt-menu__ver-arrow la la-angle-right"></i>
														</a>
														<div class="kt-menu__submenu " >
															<span class="kt-menu__arrow"></span>
															<ul class="kt-menu__subnav" style="margin-left: 30px">
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/glintp" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Data</span>
																	</a>
																</li>
																<li class="kt-menu__item " aria-haspopup="true">
																	<a href="http://sidms.budimas.com/editor/glintptype" class="kt-menu__link ">
																		<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																			<span></span>
																		</i>
																		<span class="kt-menu__link-text">Type</span>
																	</a>
																</li>
															</ul>
														</div>
													</li>
													
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
						</ul>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true">
								<a href="http://sidms.budimas.com/editor/glmontage" class="kt-menu__link">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Montage</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
												<li class="kt-menu__item " aria-haspopup="true">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon">GP</i>
						<span class="kt-menu__link-text">Geophysics</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Magnetotelluric</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmtreport" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Report</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmtstation" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Station</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmtproject" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Project</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmtresmp" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Map &amp; Cross-Section</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Microgravity &amp; Levelling</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmgreport" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Report</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmgbench" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Benchmark</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmgacq" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Acquisition</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmgcalib" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Calibration</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmgprocess" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Processing</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmglevel" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Leveling</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Microearthquake</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmeqreport" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Report</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmeqstation" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Station</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gpmeqevent" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Event</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Velocity Model</span>
												<i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gpmeqvelocity/3" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">1D</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gpmeqvelocity/2" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">2D</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gpmeqvelocity/1" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">3D</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Velocity Map &amp; Cross-Section</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</li>
												<li class="kt-menu__item " aria-haspopup="true">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon">GC</i>
						<span class="kt-menu__link-text">&nbsp;&nbsp;Geochemistry</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Main Data</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcsamplingwell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Sampling</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcncgwell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">NCG</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcbrinewell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Brine</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gccondensatewell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Condensate</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcweirboxwell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Weirbox</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcisotopewell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Isotope</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcmineralsat" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Mineral Saturation Index</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcsfmonitor" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Surface Facility Monitoring</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcsteam" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Steam Quality</span>
											</a>
										</li>
										
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gccorrosioncoupon" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Corrosion Coupon</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Calculation</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcionbalancewell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Ion Balance</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcrsvcondt" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Reservoir Condition</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gc_brine_geothermometer_calc/index" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Brine Geothermometer</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gc_gas_geothermometer_calc/index" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Gas Geothermometer</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gcssicalc" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Silica Saturarion Index</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="javascript:;" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Mineral Saturartion Index</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Monitoring</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Graphic</span>
												<i class="kt-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="kt-menu__submenu ">
												<span class="kt-menu__arrow"></span>
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gcelementvselement" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Element Vs Element</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">NCG</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Gas Vs Gas</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Elemet</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Ph</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Brine Geothermometer</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Gas Thermometer</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Silica Saturation</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Mineral Saturation</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Enthalpy</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Mixing Model</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Isotope</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Weekly Monitoring</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Steam Quality</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="javascript:;" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
																<span></span>
															</i>
															<span class="kt-menu__link-text">Corrosion Coupon</span>
														</a>
													</li>
													<li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gm/carhar" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																<span></span>
															</i>
															<span class="kt-menu__link-text">CarHar</span>
														</a>
                                                    </li>
                                                    <li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gm/ft" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																<span></span>
															</i>
															<span class="kt-menu__link-text">FTHSH</span>
														</a>
                                                    </li>
                                                    <li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gm/ftco2" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																<span></span>
															</i>
															<span class="kt-menu__link-text">FTCO2</span>
														</a>
                                                    </li>
                                                    <li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gm/fth2s" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																<span></span>
															</i>
															<span class="kt-menu__link-text">FTH2S</span>
														</a>
                                                    </li>
                                                    <li class="kt-menu__item " aria-haspopup="true">
														<a href="http://sidms.budimas.com/editor/gm/cocochco" class="kt-menu__link ">
															<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
																<span></span>
															</i>
															<span class="kt-menu__link-text">COCOCHCO2</span>
														</a>
                                                    </li>
												</ul>
											</div>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="javascript:;" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Brine</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="javascript:;" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Isotope</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="javascript:;" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Liquid Tracer</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="javascript:;" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Gas Tracer</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Reservoir Tracer Test</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gctracertestliquidwell" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Liquid Tracer Test</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/gctracerteststeam" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Steam Tracer Test</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</li>
												<li class="kt-menu__item " aria-haspopup="true">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon">RE</i>
						<span class="kt-menu__link-text">Reservoir</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Surface Monitoring</span>
									<i class="kt-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="kt-menu__submenu ">
									<span class="kt-menu__arrow"></span>
									<ul class="kt-menu__subnav">
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/resmdaily" class="kt-menu__link kt-menu__toggle">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Daily Monitoring</span>
												
											</a>
											
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/resmprodtest" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Production Test</span>
											</a>
										</li>
										<li class="kt-menu__item " aria-haspopup="true">
											<a href="http://sidms.budimas.com/editor/resminjtest" class="kt-menu__link ">
												<i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="kt-menu__link-text">Injection Test</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="http://sidms.budimas.com/editor/lm" class="kt-menu__link">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Logging Measurement</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="http://sidms.budimas.com/editor/redhpdata" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">DHP Monitoring</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
												<li class="kt-menu__item " aria-haspopup="true">
					<a href="javascript:;" class="kt-menu__link kt-menu__toggle">
						<i class="kt-menu__link-icon flaticon2-gear"></i>
						<span class="kt-menu__link-text">&nbsp;&nbsp;Setting</span>
						<i class="kt-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="kt-menu__submenu ">
						<span class="kt-menu__arrow"></span>
						<ul class="kt-menu__subnav">
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="http://sidms.budimas.com/editor/user" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">User Management</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="http://sidms.budimas.com/editor/rolediv" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Position Management</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="http://sidms.budimas.com/editor/module" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Module Setting</span>
								</a>
							</li>
							<li class="kt-menu__item " aria-haspopup="true">
								<a href="http://sidms.budimas.com/editor/action" class="kt-menu__link ">
									<i class="kt-menu__link-bullet kt-menu__link-bullet--line">
										<span></span>
									</i>
									<span class="kt-menu__link-text">Action Setting</span>
								</a>
							</li>
							
						</ul>
					</div>
				</li>
							</ul>
		<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 2px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
	</div>

	<!-- end:: Aside Menu -->
</div>