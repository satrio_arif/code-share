<?php
return [

	/*
	|--------------------------------------------------------------------------
	| Directory Paths
	|--------------------------------------------------------------------------
	|
	| This value determines the directory path for the assets you use such as
	| CSS, js, plug-ins, etc.
	| 
	*/

	'path' => [
		'public' => '/code-share/public',
		'uploads' => '/code-share/public/uploads',
		'plugins' => '/code-share/assets/plugins',
		'js' => '/code-share/assets/js',
		'css' => '/code-share/assets/css',		
		'media' => '/code-share/assets/media'
	],

];