<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;

class StudentController extends Controller
{
	public function index()
	{
		$number = 1;
		$students = Student::all();

		return view ('editor.student.index', compact('students','number'));
	}

	public function store(Request $request)
	{
		$student = new Student;
		$student->name = $request->input('name');
		$student->save();

		return redirect()->action('Editor\StudentController@index');
	}

	public function update($id, Request $request)
	{
		$student = Student::find($id);
		$student->name = $request->input('name');
		$student->save();

		return redirect()->action('Editor\StudentController@index');
	}

	public function delete($id)
	{
		Student::find($id)->delete();
		
		return redirect()->action('Editor\StudentController@index');
	}
}
